package il.ac.jce.shaifi.searchandslide;

import java.util.List;

/**
 * Created by fisher on 28/01/2016.
 */
public interface SearchImagesHandler {
    public void handleImagesList(List<String> imagesUrls);
}
